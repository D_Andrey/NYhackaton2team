package com.hackaton.demo.repository;

import com.hackaton.demo.items.Kid;

import java.util.List;

public interface KidRepository {

    List<Kid> getKids();

    void updateKid(Kid kid);
}

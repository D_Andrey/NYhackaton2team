package com.hackaton.demo.repository;

import com.hackaton.demo.items.Kid;
import com.hackaton.demo.utils.KidsDataProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class KidRepositoryDefault implements KidRepository {

    private List<Kid> kids = KidsDataProvider.fillKid();

    @Override
    public List<Kid> getKids() {
        return kids;
    }

    @Override
    public void updateKid(Kid kid) {
        kids.set(kid.getId(), kid);
    }
}

package com.hackaton.demo.utils;

import com.hackaton.demo.items.Kid;
import com.hackaton.demo.items.Wish;

import java.util.ArrayList;
import java.util.List;

public class KidsDataProvider {

    private static List<Kid> listOfKids;

    public static List<Kid> fillKid(){
        List<Wish> listOfWish = new ArrayList<>();
        listOfWish.add(new Wish("Конструктор Engino Stem Heroes Спортивные автомобили: Драгстер (SH32)",
                "https://rozetka.com.ua/engino_sh32/p17195958/"));
        listOfWish.add(new Wish("5552 Мозайка картинка Вінкс \"Лаура\" 13159034Р",
                "https://rozetka.com.ua/23507312/p23507312/)"));
        listOfKids.add(new Kid(0,"Виталий", "Тарасов",
                "Черкассы, ул. Капитана Пилипенко", 4, listOfWish));

        listOfWish = new ArrayList<>();
        listOfWish.add(new Wish("Игровой набор Hasbro Play-Doh Мистер Зубастик (B5520)",
                "Подробнее: https://rozetka.com.ua/hasbro_b5520/p9875128/"));
        listOfKids.add(new Kid(1,"Илья", "Абрамов",
                "Черкассы, ул. Капитана Пилипенко", 8, listOfWish));

        listOfWish = new ArrayList<>();
        listOfWish.add(new Wish("Набор для творчества Rosa Start Новогоднее настроение Игрушки бумажные 5 шт (4823086707443)",
                "Подробнее: https://rozetka.com.ua/rosa_start_4823086707443/p12481856/"));
        listOfWish.add(new Wish("Интерактивный Ёж Spin Master Zoomer Hedgiez гламурный Rosie (SM14408-8)",
                "Подробнее: https://rozetka.com.ua/spin_master_sm_14408_8/p24368764/"));
        listOfKids.add(new Kid(2,"Ольга", "Александрова",
                "Черкассы, ул. Капитана Пилипенко", 6, listOfWish));

        listOfWish = new ArrayList<>();
        listOfWish.add(new Wish("Kidz Delight Мой планшет-азбука (T55622)",
                "Подробнее: https://rozetka.com.ua/kidz_delight_my_planshet_azbuka_t55622/p451444/"));
        listOfWish.add(new Wish("Медведь с красным цветком музыкальный 22 см Lava (LF1096)",
                "Подробнее: https://rozetka.com.ua/bear_muz_22sm_lava_lf1096/p414849/"));
        listOfKids.add(new Kid(3,"Светлана", "Белоусова",
                "Кропивницкий, ул. Суворова, 1", 5, listOfWish));

        listOfWish = new ArrayList<>();
        listOfWish.add(new Wish("Рюкзак плюшевый Premium Toys Щенячий патруль Гонщик (PT1602004)",
                "Подробнее: https://rozetka.com.ua/premium_toys_pt1602004/p12653923/"));
        listOfWish.add(new Wish("Бластер Элит Рафкат Hasbro Nerf (A1691)",
                "Подробнее: https://rozetka.com.ua/elite_rafkat_hasbro_a1691/p275729/"));
        listOfKids.add(new Kid(4,"Михаил", "Владимиров",
                "Кропивницкий, ул. Суворова, 1", 7, listOfWish));

        return listOfKids;
    }
}

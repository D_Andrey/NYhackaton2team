package com.hackaton.demo.items;

public class Wish {

    private String name;

    private String url;

    boolean isSelect;

    public Wish(){

    }

    public Wish(String name, String url) {
        this.name = name;
        this.url = url;
        this.isSelect = false;

    }

    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }


    @Override
    public String toString() {
        return "Wish{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}

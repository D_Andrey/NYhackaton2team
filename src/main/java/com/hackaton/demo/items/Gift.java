package com.hackaton.demo.items;

public class Gift {

    private String namePresenter;

    private Wish present;

    public Gift(String presenter, Wish present) {
        this.namePresenter = presenter;
        this.present = present;
    }


    @Override
    public String toString() {
        return "Gift{" +
                "presenter=" + namePresenter +
                ", present=" + present +
                '}';
    }
}

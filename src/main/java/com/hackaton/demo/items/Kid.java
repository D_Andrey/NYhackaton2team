package com.hackaton.demo.items;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Kid implements Serializable {

    private int id;

    private String firstName;

    private String lastName;

    private int age;

    private String address;

    private List<Wish> wishList;

    public Kid() {
    }

    public Kid(int id, String firstName, String lastName, String address, int age, List<Wish> wishList) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
    }

    public List<Wish> getWishList() {
        List<Wish> myWish = new ArrayList<>();
        for (Wish item : myWish) {
            if (!item.isSelect) {
                myWish.add(item);
            }
        }
        return myWish;
    }

    public int getId() {
        return id;
    }

    public void setWishList(List<Wish> wishList) {
        this.wishList = wishList;
    }

    @Override
    public String toString() {
        return "Kid{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}

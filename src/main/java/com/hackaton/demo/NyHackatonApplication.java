package com.hackaton.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NyHackatonApplication {

	public static void main(String[] args) {
		SpringApplication.run(NyHackatonApplication.class, args);
	}
}

package com.hackaton.demo.service;

import com.hackaton.demo.items.Kid;
import com.hackaton.demo.repository.KidRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class KidServiceDefault implements KidService {

    private final KidRepository kidRepository;

    public KidServiceDefault(KidRepository kidRepository) {
        this.kidRepository = kidRepository;
    }

    @Override
    public List<Kid> getKids() {
        return kidRepository.getKids();
    }

    @Override
    public void updateKid(Kid kid) {
        kidRepository.updateKid(kid);
    }
}

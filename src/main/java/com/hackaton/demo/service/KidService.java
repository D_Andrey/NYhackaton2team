package com.hackaton.demo.service;

import com.hackaton.demo.items.Kid;

import java.util.List;

public interface KidService {

    List<Kid> getKids();

    void updateKid(Kid kid);
}

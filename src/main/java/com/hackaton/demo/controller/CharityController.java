package com.hackaton.demo.controller;

import com.hackaton.demo.items.Kid;
import com.hackaton.demo.service.KidService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class CharityController {

    private final KidService kidService;

    public CharityController(KidService kidService) {
        this.kidService = kidService;
    }

    @GetMapping("/kidsList")
    public List<Kid> getKids() {
        return kidService.getKids();
    }

    @PostMapping("/updateKid")
    public void updateKid(Kid kid) {
        kidService.updateKid(kid);
    }
}
